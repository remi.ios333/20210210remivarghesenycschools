//
//  SchoolsListTableViewController.swift
//  20210204-[RemiVarghese]-NYCSchools
//
//  Created by Remi Varghese on 2/5/21.
//  Copyright © 2021 Remi Varghese. All rights reserved.
//

import UIKit

class SchoolsListTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var schoolsTableView: UITableView!
    var schools = [School]()
    var selectedSchool: School?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        schoolsTableView.delegate = self
        schoolsTableView.dataSource = self
        title = "Loading..."
        downloadNYCSchools()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = schools[indexPath.row].schoolName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSchool = schools[indexPath.row]
        performSegue(withIdentifier: "SAT_scores", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as? SchoolsSATScoreViewController
        destinationVC!.school = selectedSchool
        
    }
    
    func downloadNYCSchools() {
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        let task = URLSession.shared.schoolDownloadTask(with: url!) { list, response, error in
            if let list = list {
                self.schools = list
                DispatchQueue.main.async {
                    self.title = "NYC Schools"
                    self.schoolsTableView.reloadData()
                }
            }
        }
        task.resume()
    }
    
    
}

