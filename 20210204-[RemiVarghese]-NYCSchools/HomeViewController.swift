//
//  HomeViewController.swift
//  20210204-[RemiVarghese]-NYCSchools
//
//  Created by Remi Varghese on 2/5/21.
//  Copyright © 2021 Remi Varghese. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBAction func ListofSchoolsbutton(_ sender: Any) {
        performSegue(withIdentifier: "schoolsList", sender: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
