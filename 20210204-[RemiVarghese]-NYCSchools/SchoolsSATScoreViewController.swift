//
//  SchoolsSATScoreViewController.swift
//  20210204-[RemiVarghese]-NYCSchools
//
//  Created by Remi Varghese on 2/8/21.
//  Copyright © 2021 Remi Varghese. All rights reserved.
//

import UIKit

class SchoolsSATScoreViewController: UIViewController {
    
    var school: School?
    
    @IBOutlet weak var satMathScore: UILabel!
    @IBOutlet weak var satReadingScore: UILabel!
    @IBOutlet weak var satWritingScore: UILabel!
    
    func downloadSATscores() {
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        let task = URLSession.shared.satScoreDownloadTask(with: url!) { sat, response, error in
            if error == nil {
                guard let sat = sat else {
                    return
                }
                self.showResults(scores: sat)
                if let encodedScores = try? JSONEncoder().encode(sat) {
                    UserDefaults.standard.set(encodedScores, forKey: "Key")
                }
            }
        }
        task.resume()
    }
    
    func showResults(scores: schoolSATscores) {
        let index = scores.firstIndex {$0.dbn == school?.dbn}
        
        guard let idx = index else {
            return
        }
        let score = scores[idx]
        title = school?.schoolName
        satMathScore.text = "Math: \(score.satMathAvgScore)"
        satReadingScore.text = "Reading: \(score.satCriticalReadingAvgScore)"
        satWritingScore.text = "Writing: \(score.satWritingAvgScore)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Loading..."
        guard let encodedScores =  UserDefaults.standard.object(forKey: "Key") as? Data,
            let scores = try? JSONDecoder().decode(schoolSATscores.self, from: encodedScores) else {
                downloadSATscores()
                return
        }
        showResults(scores: scores)
    }
    
}
