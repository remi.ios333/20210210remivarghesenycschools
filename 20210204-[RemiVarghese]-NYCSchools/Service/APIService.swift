//
//  APIService.swift
//  20210204-[RemiVarghese]-NYCSchools
//
//  Created by Remi Varghese on 2/8/21.
//  Copyright © 2021 Remi Varghese. All rights reserved.

import Foundation

// MARK: - APIServiceElement
struct School: Codable {
    let dbn, schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
    }
}

struct schoolSATscore: Codable {
    let satCriticalReadingAvgScore, satMathAvgScore, satWritingAvgScore: String
    let dbn: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
    
}

typealias Schools = [School]
typealias schoolSATscores = [schoolSATscore]

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? JSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func schoolDownloadTask(with url: URL, completionHandler: @escaping (Schools?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func satScoreDownloadTask(with url: URL, completionHandler: @escaping (schoolSATscores?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}



